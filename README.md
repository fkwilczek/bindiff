# bindiff

Terminal program to highlight differences between two binary files.

## Author

Filip K

## License

This project is licensed under the AGPLv3 License - see the LICENSE file for details.