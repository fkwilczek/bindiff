use clap::Parser;
use itertools::{EitherOrBoth::*, Itertools};
use std::cmp::max;
use std::fs::File;
use std::io;
use std::os::unix::prelude::FileExt;
use std::path::PathBuf;
use std::process::exit;
use termion::{color, cursor};

#[derive(Parser, Debug)]
#[command(author = "Filip K", version="1.0.1", about="Highlights binary diffrences of two files with color", long_about = None)]
pub struct Args {
	/// First file to compare
	#[arg()]
	file1_path: PathBuf,

	/// Second file to compare
	#[arg()]
	file2_path: PathBuf,

	/// Number of bytes compared in each line
	#[arg(short, long)]
	column_width: Option<u16>,

	/// Offset
	#[arg(short, long, default_value_t = 0)]
	offset: usize,
}

pub struct BinDiffPrinter {
	file1: File,
	file2: File,
	column_width: u16,
	offset_digits: u8,
	start_offset: usize,
}

impl BinDiffPrinter {
	fn print_column_offsets(&self, offset: usize) {
		for i in 0..self.column_width {
			print!("{:02X}", (i + offset as u16) % 256);
			if (i + 1) % 4 == 0 {
				print!(" ");
			}
		}

		if self.column_width % 4 == 0 {
			print!("{}", cursor::Left(1));
		}
	}

	fn print_row_offset(&self, offset: usize) {
		print!("{:0zeros$X}", offset, zeros = self.offset_digits as usize);
	}

	fn print_header(&self, offset: usize) {
		print!("{}", " ".repeat(self.offset_digits as usize));
		print!("  ");
		self.print_column_offsets(offset);
		print!("  ");
		self.print_column_offsets(offset);
		print!("\n\n");
	}

	fn print_row(&self, bytes: &[u8], bytes2: &[u8], offset: usize) {
		self.print_row_offset(offset);
		print!("  ");
		self.print_row_data(bytes, bytes2);
	}

	fn print_rows_from_files(&self) -> io::Result<()> {
		let mut buf1: Vec<u8> = vec![0; self.column_width as usize];
		let mut buf2: Vec<u8> = vec![0; self.column_width as usize];
		let mut offset: usize = self.start_offset;
		self.print_header(offset);

		loop {
			let readed_bytes1 = self.file1.read_at(&mut buf1, offset as u64)?;
			let readed_bytes2 = self.file2.read_at(&mut buf2, offset as u64)?;
			if readed_bytes1 == 0 && readed_bytes2 == 0 {
				break;
			}

			self.print_row(&buf1[0..readed_bytes1], &buf2[0..readed_bytes2], offset);
			println!();
			offset += self.column_width as usize;
		}

		Ok(())
	}

	fn print_row_hex(&self, bytes: &[u8], bytes_cmp: &[u8]) {
		for (i, pair) in bytes.iter().zip_longest(bytes_cmp).enumerate() {
			let byte = match pair {
				Right(_) => {
					print!("  ");

					if (i + 1) % 4 == 0 {
						print!(" ")
					}

					continue;
				}

				Left(byte) => {
					print!("{}", color::Fg(color::Red));
					byte
				}

				Both(byte1, byte2) => {
					if_differs_change_red_or_default(byte1, byte2);
					byte1
				}
			};

			print!("{:02X}", byte);

			if (i + 1) % 4 == 0 {
				print!(" ")
			}
		}

		let max_size = max(bytes.len(), bytes_cmp.len());
		if max_size % 4 == 0 {
			print!("{}", cursor::Left(1));
		}

		print!("{}", color::Fg(color::Reset));

		if self.column_width as usize <= max_size {
			return;
		}

		let needed_separator_count = (max(self.column_width, 1) - 1) / 4;
		let printed_separators_count = (max(max_size, 1) - 1) / 4;
		let separators_to_print = needed_separator_count as usize - printed_separators_count;
		let space_replacement_to_print = (self.column_width as usize - max_size) * 2;
		print!(
			"{}",
			" ".repeat(separators_to_print + space_replacement_to_print)
		)
	}

	fn print_row_data(&self, bytes: &[u8], bytes2: &[u8]) {
		self.print_row_hex(bytes, bytes2);
		print!("  ");
		self.print_row_hex(bytes2, bytes);
		print!("  ");
		self.print_row_ascii(bytes, bytes2);
		print!("  ");
		self.print_row_ascii(bytes2, bytes);
	}

	fn print_row_ascii(&self, bytes: &[u8], bytes_cmp: &[u8]) {
		for pair in bytes.iter().zip_longest(bytes_cmp.iter()) {
			let byte = match pair {
				Right(_) => {
					print!(" ");
					continue;
				}

				Left(byte) => {
					print!("{}", color::Fg(color::Red));
					byte
				}

				Both(byte1, byte2) => {
					if_differs_change_red_or_default(byte1, byte2);
					byte1
				}
			};

			print!("{}", to_ascii_graphic_char_or(byte, &'.'));
		}

		print!("{}", color::Fg(color::Reset));
		let max_size = max(bytes.len(), bytes_cmp.len());
		if self.column_width as usize <= max_size {
			return;
		}

		let space_replacement_to_print = self.column_width as usize - max_size;
		print!("{}", " ".repeat(space_replacement_to_print));
	}
}

fn to_ascii_graphic_char_or(byte: &u8, replacemant_char: &char) -> char {
	if !byte.is_ascii_graphic() {
		return *replacemant_char;
	}

	char::from_u32(*byte as u32).unwrap_or(*replacemant_char)
}

fn if_differs_change_red_or_default(byte: &u8, byte2: &u8) {
	if byte == byte2 {
		print!("{}", color::Fg(color::Reset));
	} else {
		print!("{}", color::Fg(color::Red));
	}
}

fn get_decimal_digits(number: usize) -> u8 {
	(number.ilog(16) + 1) as u8
}

fn get_max_size(file1: &File, file2: &File) -> Result<usize, io::Error> {
	let size1 = file1.metadata()?.len();
	let size2 = file2.metadata()?.len();
	Ok(max(size1, size2) as usize)
}

fn calculate_optimal_width(max_offset_digits: u8) -> u16 {
	let terminal_width = termion::terminal_size().unwrap_or((0, 0)).0;
	if terminal_width < 4 + max_offset_digits as u16 {
		return 4;
	}

	let space_for_bytes = terminal_width - 6 - max_offset_digits as u16;
	max(space_for_bytes / (6 * 4 + 2), 1) * 4
}

/// Print binary diffrences exit on error
pub fn print_binary_diffrences(args: Args) {
	let file1 = match File::open(&args.file1_path) {
		Ok(file) => file,
		Err(err) => {
			eprintln!(
				"bindiff: {}: {}",
				args.file1_path.to_str().unwrap_or(""),
				err
			);

			exit(1);
		}
	};

	let file2 = match File::open(&args.file2_path) {
		Err(err) => {
			eprintln!(
				"bindiff: {}: {}",
				args.file2_path.to_str().unwrap_or(""),
				err
			);

			exit(1);
		}

		Ok(file) => file,
	};

	let max_size = match get_max_size(&file1, &file2) {
		Ok(value) => value,
		Err(err) => {
			eprint!("bindiff: Failed to read files sizes: {}", err);
			exit(2);
		}
	};

	let max_offset_digits = get_decimal_digits(max_size);
	let bin_diff_printer = BinDiffPrinter {
		file1,
		file2,
		offset_digits: max_offset_digits,
		start_offset: args.offset,
		column_width: args.column_width.unwrap_or_else(|| {
			max_size.min(calculate_optimal_width(max_offset_digits) as usize) as u16
		}),
	};

	if let Err(err) = bin_diff_printer.print_rows_from_files() {
		eprint!("bindiff: Failed to print rows: {}", err)
	};
}
