use bindiff::{print_binary_diffrences, Args};
use clap::Parser;

fn main() {
	let args = Args::parse();
	print_binary_diffrences(args);
}
